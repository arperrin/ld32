/// scr_init_game()
/// initializes game variables, globals, etc.

// GLOBALS
global.DEBUG = false;
global.grid_size = 32;
global.right_wall = room_width - global.grid_size;
global.left_wall = 0 + global.grid_size;
global.top_wall = 0 + global.grid_size * 2;
global.bottom_wall = room_height - global.grid_size;
global.god_mode = false;
global.detection_radius = false;
global.spawning = true;

// Randomize
randomize();

// Debug Menu
if (global.DEBUG)
{
  instance_create(32,64,obj_debug_menu);
}

// Spawner
instance_create(0,0,obj_enemy_spawner);

// Hud
instance_create(0,0,obj_hud)

// Spawn 2 crates, randomly
var lb = global.left_wall + global.grid_size *2;
var rb = global.right_wall - global.grid_size *2;
var bb = global.bottom_wall - global.grid_size*2;
var tb = global.top_wall + global.grid_size*2;
var cx = (irandom((rb - lb)/global.grid_size) + 2) * global.grid_size;
var cy = (irandom((bb - tb)/global.grid_size) + 2) * global.grid_size;
var c1 = instance_create(cx, cy, obj_junk);
with (c1)
{
  image_index = 9;
}

cx = irandom((rb - lb)/global.grid_size) * global.grid_size;
cy = irandom((bb - tb)/global.grid_size) * global.grid_size;
c1 = instance_create(cx, cy, obj_junk);
with (c1)
{
  image_index = 9;
}

audio_play_sound(music,2,true);