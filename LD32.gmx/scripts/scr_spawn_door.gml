///scr_spawn_door
// Create a spawner door

with (obj_enemy_spawner)
{
  if (door_index <= max_door_index){
    var i = ds_list_find_value(doors_list, door_index++);
    show_debug_message("spawning door at " + string(doors[i,0]) + ", " + string(doors[i,1]));
    var new_door = instance_create(doors[i, 0], doors[i, 1], obj_spawner_door);
    new_door.spawn_x = doors[i,2];
    new_door.spawn_y = doors[i,3];
  }
}
