/// scr_spawn_floor()
// Create a spawner floor

with (obj_enemy_spawner)
{
  if (floor_count < max_floor_count){
    var fx = irandom(room_width / global.grid_size) * global.grid_size;
    var fy = irandom(room_height / global.grid_size) * global.grid_size;
    
    if (instance_place(fx, fy, obj_spawner_floor)
    || instance_place(fx, fy, obj_robot)
    || instance_place(fx, fy, obj_junk)
    || instance_place(fx, fy, obj_player)
    || fx < global.left_wall
    || fx >= global.right_wall
    || fy < global.top_wall
    || fy >= global.bottom_wall)
    {
      // 
    }
    else
    {
      show_debug_message("Creating a floor spawner at " + string(fx) + ", " + string(fy));
      instance_create(fx, fy, obj_spawner_floor);
      floor_count++;

    }
  }
}
