Ludum Dare 32: Theme: An  Unconventional Weapon
Top down dual stick. Character has a gravity gun type weapon, and is a walking nuke. If he dies, the world blows up.
Pick up objects with the gravity gun. Throw them at robots to blow them apart, and use the parts and pieces that come off of the
blown up robots to throw them at other robots. Stay alive.

To Do --
Day 1: Saturday
11:00 am
[x] Level with a viewpoint
[x] Player moving on screen
[x] Player can pick up and throw objects
Breakfast somewhere in there...
3:24 pm - short break...(can I afford this?)
3:48 pm - break over
[x] Enemies on screen
[x] Enemies damaged by thrown objects
[o] Thrown objects break on other objects - They get destroyed, but don't leave debris
[x] Enemies blow apart into more pieces
5 pm -- food?
6:30 pm - back
[x] Enemies moving on screen
[x] Enemies moving intelligently on screen
[x] Player gets damaged
8 pm - break
8:49 pm - back
[o] Player can die - Setup, but not quite there yet.
[x] Enemy spawner
[x] Debug menu?
12:58 am - tired!

Day 2:
10:30 am
[x] GUI
12:23 pm
[x] Art
[x] Character holding the gravity gun
[x] Thrown objects break on other objects
[x] Additional junk art
[x] Thrown objects break into debris
2:42 pm - shower, dinner, whatnot
[--] Enemy attacks are visualized.
[o] Enemy score - Should add a bonus/combo structure
[] Melee attacks?
[x] Coolant
[x] Health
[] Sound!!!
	Melee Swing
	Vacuum
	Fire
	Crash/explosion
	Junk landing
	Explosion of planet
[] Balance

Nice to Have:
Additional weapon types
Additional enemy types
"Juice"

Bugs:
[x] Enemies damaged when collide with clutter on the ground.
[] Had a crash where a crate wouldn't get picked up. Don't really know what that was about...Can't reproduce.
It seemed like an instance got set to "loaded_shot" when it wasn't a valid instance of junk.